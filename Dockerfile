#Use an existing docker image as a base 
FROM node:alpine as builder
#
WORKDIR '/app'

COPY package.json .
RUN npm install

COPY . .
# Download and install a depen
RUN npm run build

FROM nginx

EXPOSE 80

COPY --from=builder /app/build /usr/share/nginx/html
##Tell the image wh to do when it starts as container
